<?php

namespace CustomLibraries\Utilities\DataConfig\PropertyImage;

use CustomLibraries\Utilities\DataConfig\Interfaces\PropertyImage;
use Carbon\Carbon;

class PropertyImageConfig  implements PropertyImage {

    const IMAGE_ID = 'image_id';
    const IMAGE_PROPERTY_TYPE_ID = 'property_type_id';
    const IMAGE_TITLE = 'title';
    const IMAGE_DESCRIPTION = 'description';
    const IMAGE_URL = 'url';
    const IMAGE_PHOTOGRAPHER = 'photographer_attribution_url';
    const IMAGE_DOWNLOAD_URL = 'download_url';
    const IMAGE_CREATED_AT = 'created_at';
    const IMAGE_UPDATED_AT = 'updated_at';
    const DEFAULT_IMAGE_ID = 'no-image-id_';
    const DEFAULT_IMAGE_PROPERTY_ID = 0;
    const DEFAULT_IMAGE_TITLE = "default-image-tittle";
    const DEFAULT_IMAGE_PHOTOGRAPHER = "https://www.example.com";
    const DEFAULT_IMAGE_DESCRIPTION = "Default image description";
    const DEFAULT_IMAGE_URL = "https://picsum.photos/seed/picsum/200/300";
    const DEFAULT_IMAGE_DOWNLOAD_URL = "https://picsum.photos/seed/picsum/200/300";

    private string $imageId;
    private string $imagePropertyTypeId;
    private string $imageTitle;
    private string $imageDescription;
    private string $imageUrl;
    private string $imagePhotographer;
    private string $imageDownloadUrl;
    private mixed $imageCreatedAt;
    private mixed $imageUpdatedAt;


    public function __construct()
    {
        $this->setImageCreatedAt(Carbon::now());
        $this->setImageUpdatedAt(Carbon::now());
        $this->setImageTitle(SELF::DEFAULT_IMAGE_TITLE);
        $this->setImageDescription(SELF::DEFAULT_IMAGE_DESCRIPTION);
    }

    public function getData() : array
    {
        return [
            SELF::IMAGE_ID => $this->imageId,
            SELF::IMAGE_PROPERTY_TYPE_ID => $this->imagePropertyTypeId,
            SELF::IMAGE_TITLE => $this->imageTitle,
            SELF::IMAGE_DESCRIPTION => $this->imageDescription,
            SELF::IMAGE_URL => $this->imageUrl,
            SELF::IMAGE_PHOTOGRAPHER => $this->imagePhotographer,
            SELF::IMAGE_DOWNLOAD_URL => $this->imageDownloadUrl,
            SELF::IMAGE_CREATED_AT => $this->imageCreatedAt,
            SELF::IMAGE_UPDATED_AT => $this->imageUpdatedAt,
        ];
    }

    /**
     * @return string
     */
    public function getImageId(): string
    {
        return $this->imageId;
    }


    /**
     * @param string $imageId
     */
    public function setImageId(string $imageId)
    {
        $this->imageId = $imageId;

    }

    /**
     * @return string
     */
    public function getImageTitle(): string
    {
        return $this->imageTitle;
    }

    /**
     * @param string $imageTitle
     */
    public function setImageTitle(string $imageTitle)
    {
        $this->imageTitle = $imageTitle;
    }

    /**
     * @return string
     */
    public function getImageDescription(): string
    {
        return $this->imageDescription;
    }

    /**
     * @param string $imageDescription
     */
    public function setImageDescription(string $imageDescription)
    {
        $this->imageDescription = $imageDescription;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return string
     */
    public function getImagePhotographer(): string
    {
        return $this->imagePhotographer;
    }

    /**
     * @param string $imagePhotographer
     */
    public function setImagePhotographer(string $imagePhotographer)
    {
        $this->imagePhotographer = $imagePhotographer;
    }

    /**
     * @return string
     */
    public function getImageDownloadUrl(): string
    {
        return $this->imageDownloadUrl;
    }

    /**
     * @param string $imageDownloadUrl
     */
    public function setImageDownloadUrl(string $imageDownloadUrl)
    {
        $this->imageDownloadUrl = $imageDownloadUrl;
    }

    /**
     * @return mixed
     */
    public function getImageCreatedAt(): mixed
    {
        return $this->imageCreatedAt;
    }

    /**
     * @param mixed $imageCreatedAt
     */
    public function setImageCreatedAt(mixed $imageCreatedAt)
    {
        $this->imageCreatedAt = $imageCreatedAt;
    }

    /**
     * @return mixed
     */
    public function getImageUpdatedAt(): mixed
    {
        return $this->imageUpdatedAt;
    }

    /**
     * @param mixed $imageUpdatedAt
     */
    public function setImageUpdatedAt(mixed $imageUpdatedAt)
    {
        $this->imageUpdatedAt = $imageUpdatedAt;
    }

    /**
     * @return string
     */
    public function getImagePropertyTypeId(): string
    {
        return $this->imagePropertyTypeId;
    }


    /**
     * @param string $imagePropertyTypeId
     */
    public function setImagePropertyTypeId(string $imagePropertyTypeId)
    {
        $this->imagePropertyTypeId = $imagePropertyTypeId;
    }

}
