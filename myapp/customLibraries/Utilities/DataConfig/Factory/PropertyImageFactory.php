<?php

namespace CustomLibraries\Utilities\DataConfig\Factory;

use CustomLibraries\Utilities\DataConfig\PropertyImage\PropertyImageConfig;
use Ramsey\Uuid\Uuid;

class PropertyImageFactory {

    public function __construct()
    {

    }

    public function create() : PropertyImageConfig
    {

        return New PropertyImageConfig();

    }

    public function process($config)
    {

        return $this
            ->create()
           ->setImageId(!isset($config[PropertyImageConfig::IMAGE_ID]) ?
            PropertyImageConfig::DEFAULT_IMAGE_ID.Uuid::uuid4()
            : $config[PropertyImageConfig::IMAGE_ID])
            ->setImagePropertyTypeId(!isset($config[PropertyImageConfig::IMAGE_PROPERTY_TYPE_ID]) ?
                PropertyImageConfig::DEFAULT_IMAGE_PROPERTY_ID
                : $config[PropertyImageConfig::IMAGE_PROPERTY_TYPE_ID])
            ->setImageTitle(!isset($config[PropertyImageConfig::IMAGE_TITLE]) ?
            PropertyImageConfig::DEFAULT_IMAGE_TITLE
            : $config[PropertyImageConfig::IMAGE_TITLE])
           ->setImageDescription(!isset($config[PropertyImageConfig::IMAGE_DESCRIPTION]) ?
            PropertyImageConfig::DEFAULT_IMAGE_DESCRIPTION
               : $config[PropertyImageConfig::IMAGE_DESCRIPTION])
            ->setImageUrl(!isset($config[PropertyImageConfig::IMAGE_URL]) ?
                PropertyImageConfig::DEFAULT_IMAGE_URL
                : $config[PropertyImageConfig::IMAGE_URL])
           ->setImagePhotographer(!isset($config[PropertyImageConfig::IMAGE_PHOTOGRAPHER]) ?
            PropertyImageConfig::DEFAULT_IMAGE_PHOTOGRAPHER
               : $config[PropertyImageConfig::IMAGE_PHOTOGRAPHER])
            ->setImageDownloadUrl(!isset($config[PropertyImageConfig::IMAGE_DOWNLOAD_URL]) ?
                PropertyImageConfig::DEFAULT_IMAGE_DOWNLOAD_URL
                : $config[PropertyImageConfig::IMAGE_DOWNLOAD_URL]);

    }

}
