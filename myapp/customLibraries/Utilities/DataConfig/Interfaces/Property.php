<?php

namespace CustomLibraries\Utilities\DataConfig\Interfaces;

use CustomLibraries\Utilities\DataConfig\Properties\PropertiesConfig;

interface Property
{

    public function getData() : array;
    public function getPropertyTypeId(): int;
    public function setPropertyTypeId(int $propertyTypeId): PropertiesConfig;
    public function getPropertyCountryId(): int;
    public function setPropertyCountryId(int $propertyCountryId): PropertiesConfig;
    public function getPropertyStateId(): int;
    public function setPropertyStateId(int $propertyStateId): PropertiesConfig;
    public function getPropertyDistrictId(): int;
    public function setPropertyDistrictId(int $propertyDistrictId): PropertiesConfig;
    public function getPropertyHeadline(): string;
    public function setPropertyHeadline(string $propertyHeadline): PropertiesConfig;
    public function getPropertyDescription(): string;
    public function setPropertyDescription(string $propertyDescription): PropertiesConfig;
    public function getPropertyAddress(): string;
    public function setPropertyAddress(string $propertyAddress): PropertiesConfig;
    public function getPropertyImageId(): int;
    public function setPropertyImageId(int $propertyImageId): PropertiesConfig;
    public function getPropertyLatitude(): float;
    public function setPropertyLatitude(float $propertyLatitude): PropertiesConfig;
    public function getPropertyLongitude(): float;
    public function setPropertyLongitude(float $propertyLongitude): PropertiesConfig;
    public function getPropertyNoOfBedroom(): int;
    public function setPropertyNoOfBedroom(int $propertyNoOfBedroom): PropertiesConfig;
    public function getPropertyNoOfBathroom(): int;
    public function setPropertyNoOfBathroom(int $propertyNoOfBathroom): PropertiesConfig;
    public function getPropertyPrice(): float;
    public function setPropertyPrice(float $propertyPrice): PropertiesConfig;
    public function getPropertyIntentId(): int;
    public function setPropertyIntentId(int $propertyIntentId): PropertiesConfig;
    public function getPropertyShowStatusId(): int;
    public function setPropertyShowStatusId(int $propertyShowStatusId): PropertiesConfig;
    public function getPropertyCreatedAt(): mixed;
    public function setPropertyCreatedAt(mixed $propertyCreatedAt): PropertiesConfig;
    public function getPropertyUpdatedAt(): mixed;
    public function setPropertyUpdatedAt(mixed $propertyUpdatedAt): PropertiesConfig;

}
