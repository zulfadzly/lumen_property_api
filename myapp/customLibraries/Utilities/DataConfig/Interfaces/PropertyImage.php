<?php

namespace CustomLibraries\Utilities\DataConfig\Interfaces;

interface PropertyImage
{

    public function getData() : array ;
    public function getImageId();
    public function setImageId(string $imageId);
    public function getImageTitle();
    public function setImageTitle(string $imageTitle);
    public function getImageDescription();
    public function setImageDescription(string $imageDescription);
    public function getImageUrl();
    public function setImageUrl(string $imageUrl);
    public function getImagePhotographer();
    public function setImagePhotographer(string $imagePhotographer);
    public function getImageDownloadUrl();
    public function setImageDownloadUrl(string $imageDownloadUrl);
    public function getImageCreatedAt();
    public function setImageCreatedAt(mixed $imageCreatedAt);
    public function getImageUpdatedAt();
    public function setImageUpdatedAt(mixed $imageUpdateAt);
    public function setImagePropertyTypeId(string $propertyTypeId);
    public function getImagePropertyTypeId();

}
