<?php

namespace CustomLibraries\Utilities\DataConfig\Properties;

use CustomLibraries\Utilities\DataConfig\Interfaces\Property;
use Carbon\Carbon;

class PropertiesConfig implements Property {

    const PROPERTY_TYPE_ID = 'property_type_id';
    const PROPERTY_COUNTRY_ID = 'country_id';
    const PROPERTY_STATE_ID = 'state_id';
    const PROPERTY_DISTRICT_ID = 'district_id';
    const PROPERTY_HEADLINE = 'headline';
    const PROPERTY_DESCRIPTION = 'description';
    const PROPERTY_ADDRESS = 'address';
    const PROPERTY_IMAGE_ID = 'image_id';
    const PROPERTY_LATITUDE = 'latitude';
    const PROPERTY_LONGITUDE = 'longitude';
    const PROPERTY_NUMBER_OF_BEDROOM = 'number_of_bedroom';
    const PROPERTY_NUMBER_OF_BATHROOM = 'number_of_bathroom';
    const PROPERTY_PRICE = 'price';
    const PROPERTY_INTENT_ID = 'intent_id';
    const PROPERTY_SHOW_STATUS_ID = 'show_status_id';
    const PROPERTY_CREATED_AT = 'created_at';
    const PROPERTY_UPDATED_AT = 'updated_at';
    const DEFAULT_PROPERTY_HEADLINE = 'Buy any home in comfy in Kuala Lumpur with us and receive a 50% commission
    refund.';
    const DEFAULT_PROPERTY_DESCRIPTION = 'Gorgeous 4-bedroom, 2-bathroom home in beautiful Silver Lake. This property
    offers 1,160 square feet of living space and a lot size of 5,499 square feet. Your family and loved ones will enjoy
    the spacious backyard,perfect for family gatherings! Come and take a look at this beauty....Dont miss out!';

    private int $propertyTypeId;
    private int $propertyCountryId;
    private int $propertyStateId;
    private string $propertyHeadline;
    private int $propertyDistrictId;
    private string $propertyDescription;
    private string $propertyAddress;
    private int $propertyImageId;
    private float $propertyLatitude;
    private float $propertyLongitude;
    private int $propertyNoOfBedroom;
    private int $propertyNoOfBathroom;
    private float $propertyPrice;
    private int $propertyIntentId;
    private int $propertyShowStatusId;
    private mixed $propertyCreatedAt;
    private mixed $propertyUpdatedAt;

    public function __construct()
    {
        $this->setPropertyCreatedAt(Carbon::now());
        $this->setPropertyUpdatedAt(Carbon::now());
        $this->setPropertyHeadline(SELF::DEFAULT_PROPERTY_HEADLINE);
        $this->setPropertyDescription(SELF::DEFAULT_PROPERTY_DESCRIPTION);

    }

    public function getData() : array
    {
        return [
            SELF::PROPERTY_TYPE_ID => $this->propertyTypeId,
            SELF::PROPERTY_COUNTRY_ID => $this->propertyCountryId,
            SELF::PROPERTY_STATE_ID => $this->propertyStateId,
            SELF::PROPERTY_DISTRICT_ID => $this->propertyDistrictId,
            SELF::PROPERTY_HEADLINE => $this->propertyHeadline,
            SELF::PROPERTY_DESCRIPTION => $this->propertyDescription,
            SELF::PROPERTY_ADDRESS => $this->propertyAddress,
            SELF::PROPERTY_IMAGE_ID => $this->propertyImageId,
            SELF::PROPERTY_LATITUDE => $this->propertyLatitude,
            SELF::PROPERTY_LONGITUDE => $this->propertyLongitude,
            SELF::PROPERTY_NUMBER_OF_BEDROOM => $this->propertyNoOfBedroom,
            SELF::PROPERTY_NUMBER_OF_BATHROOM => $this->propertyNoOfBathroom,
            SELF::PROPERTY_PRICE => $this->propertyPrice,
            SELF::PROPERTY_INTENT_ID => $this->propertyIntentId,
            SELF::PROPERTY_SHOW_STATUS_ID => $this->propertyShowStatusId,
            SELF::PROPERTY_CREATED_AT => $this->propertyCreatedAt,
            SELF::PROPERTY_UPDATED_AT => $this->propertyUpdatedAt
        ];
    }

    /**
     * @return int
     */
    public function getPropertyTypeId(): int
    {
        return $this->propertyTypeId;
    }

    /**
     * @param int $propertyTypeId
     * @return PropertiesConfig
     */
    public function setPropertyTypeId(int $propertyTypeId): PropertiesConfig
    {
        $this->propertyTypeId = $propertyTypeId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyCountryId(): int
    {
        return $this->propertyCountryId;
    }

    /**
     * @param int $propertyCountryId
     * @return PropertiesConfig
     */
    public function setPropertyCountryId(int $propertyCountryId): PropertiesConfig
    {
        $this->propertyCountryId = $propertyCountryId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyStateId(): int
    {
        return $this->propertyStateId;
    }

    /**
     * @param int $propertyStateId
     * @return PropertiesConfig
     */
    public function setPropertyStateId(int $propertyStateId): PropertiesConfig
    {
        $this->propertyStateId = $propertyStateId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyDistrictId(): int
    {
        return $this->propertyDistrictId;
    }

    /**
     * @param int $propertyDistrictId
     * @return PropertiesConfig
     */
    public function setPropertyDistrictId(int $propertyDistrictId): PropertiesConfig
    {
        $this->propertyDistrictId = $propertyDistrictId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyHeadline(): string
    {
        return $this->propertyHeadline;
    }

    /**
     * @param string $propertyHeadline
     * @return PropertiesConfig
     */
    public function setPropertyHeadline(string $propertyHeadline): PropertiesConfig
    {
        $this->propertyHeadline = $propertyHeadline;
        return $this;
    }


    /**
     * @return string
     */
    public function getPropertyDescription(): string
    {
        return $this->propertyDescription;
    }

    /**
     * @param string $propertyDescription
     * @return PropertiesConfig
     */
    public function setPropertyDescription(string $propertyDescription): PropertiesConfig
    {
        $this->propertyDescription = $propertyDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyAddress(): string
    {
        return $this->propertyAddress;
    }

    /**
     * @param string $propertyAddress
     * @return PropertiesConfig
     */
    public function setPropertyAddress(string $propertyAddress): PropertiesConfig
    {
        $this->propertyAddress = $propertyAddress;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyImageId(): int
    {
        return $this->propertyImageId;
    }

    /**
     * @param int $propertyImageId
     * @return PropertiesConfig
     */
    public function setPropertyImageId(int $propertyImageId): PropertiesConfig
    {
        $this->propertyImageId = $propertyImageId;
        return $this;
    }

    /**
     * @return float
     */
    public function getPropertyLatitude(): float
    {
        return $this->propertyLatitude;
    }

    /**
     * @param float $propertyLatitude
     * @return PropertiesConfig
     */
    public function setPropertyLatitude(float $propertyLatitude): PropertiesConfig
    {
        $this->propertyLatitude = $propertyLatitude;
        return $this;
    }

    /**
     * @return float
     */
    public function getPropertyLongitude(): float
    {
        return $this->propertyLongitude;
    }

    /**
     * @param float $propertyLongitude
     * @return PropertiesConfig
     */
    public function setPropertyLongitude(float $propertyLongitude): PropertiesConfig
    {
        $this->propertyLongitude = $propertyLongitude;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyNoOfBedroom(): int
    {
        return $this->propertyNoOfBedroom;
    }

    /**
     * @param int $propertyNoOfBedroom
     * @return PropertiesConfig
     */
    public function setPropertyNoOfBedroom(int $propertyNoOfBedroom): PropertiesConfig
    {
        $this->propertyNoOfBedroom = $propertyNoOfBedroom;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyNoOfBathroom(): int
    {
        return $this->propertyNoOfBathroom;
    }

    /**
     * @param int $propertyNoOfBathroom
     * @return PropertiesConfig
     */
    public function setPropertyNoOfBathroom(int $propertyNoOfBathroom): PropertiesConfig
    {
        $this->propertyNoOfBathroom = $propertyNoOfBathroom;
        return $this;
    }

    /**
     * @return float
     */
    public function getPropertyPrice(): float
    {
        return $this->propertyPrice;
    }

    /**
     * @param float $propertyPrice
     * @return PropertiesConfig
     */
    public function setPropertyPrice(float $propertyPrice): PropertiesConfig
    {
        $this->propertyPrice = $propertyPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyIntentId(): int
    {
        return $this->propertyIntentId;
    }

    /**
     * @param int $propertyIntentId
     * @return PropertiesConfig
     */
    public function setPropertyIntentId(int $propertyIntentId): PropertiesConfig
    {
        $this->propertyIntentId = $propertyIntentId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPropertyShowStatusId(): int
    {
        return $this->propertyShowStatusId;
    }

    /**
     * @param int $propertyShowStatusId
     * @return PropertiesConfig
     */
    public function setPropertyShowStatusId(int $propertyShowStatusId): PropertiesConfig
    {
        $this->propertyShowStatusId = $propertyShowStatusId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPropertyCreatedAt(): mixed
    {
        return $this->propertyCreatedAt;
    }

    /**
     * @param mixed $propertyCreatedAt
     * @return PropertiesConfig
     */
    public function setPropertyCreatedAt(mixed $propertyCreatedAt): PropertiesConfig
    {
        $this->propertyCreatedAt = $propertyCreatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPropertyUpdatedAt(): mixed
    {
        return $this->propertyUpdatedAt;
    }

    /**
     * @param mixed $propertyUpdatedAt
     * @return PropertiesConfig
     */
    public function setPropertyUpdatedAt(mixed $propertyUpdatedAt): PropertiesConfig
    {
        $this->propertyUpdatedAt = $propertyUpdatedAt;
        return $this;
    }



}
