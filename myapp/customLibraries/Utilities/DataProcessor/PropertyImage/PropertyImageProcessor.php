<?php

namespace CustomLibraries\Utilities\DataProcessor\PropertyImage;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use ReflectionClass;
use ReflectionException;
use CustomLibraries\Utilities\DataConfig\PropertyImage\PropertyImageConfig;
use CustomLibraries\Utilities\DataConfig\Factory\PropertyImageFactory;
use stdClass;

class PropertyImageProcessor {

    const PROPERTY_IMAGES_TABLE = 'property_images';
    const PROPERTY_TYPE_TABLE = 'property_type';

    public function process() {

        $initImageData = [];
        $imageDataReflection = new ReflectionClass("CustomLibraries\Utilities\DataProvider\UnsplashData\UnsplashData");
        try {

            $propertyTypeData = DB::table(SELF::PROPERTY_TYPE_TABLE)
                ->select('id', 'name')
                ->get();
            $imageDataReflectionObjects = $imageDataReflection->newInstanceWithoutConstructor();


            if($propertyTypeData->count() > 0) {

                foreach ($propertyTypeData as $propertyTypeDatum) {

                    $imageData = $imageDataReflectionObjects->getImages($propertyTypeDatum->name);

                    foreach ($imageData as $imageDatum) {

                        if(!$this->isImageExist($imageDatum->id, $propertyTypeDatum->id)) {

                              $config = $this->createImageData(
                                  $propertyTypeDatum->id,
                                  $imageDatum
                              );

                              $initImageData[] = $config->getData();

                        }

                    }

                }

                //Store main property main image
                DB::table(SELF::PROPERTY_IMAGES_TABLE)
                    ->insert($initImageData);

            }

        } catch (ReflectionException $exception) {

            echo $exception->getMessage();

        }

    }


    private function isImageExist(string $imageId, int $propertyTypeId) : bool {

        $image = DB::table(SELF::PROPERTY_IMAGES_TABLE)
            ->select(DB::raw('COUNT(id) AS cnt'))
            ->where('image_id', '=', $imageId)
            ->where('property_type_id', '=', $propertyTypeId)
            ->get();
        return $image->first()->cnt > 0;

    }

    private function createImageData(int $propertyTypeId, stdClass $data) : PropertyImageConfig
    {
        $configFactory = New PropertyImageFactory();
        $config = $configFactory->create();
        $config->setImageId($data->id);
        $config->setImagePropertyTypeId($propertyTypeId);
        if(is_string($data->description)){
            $config->setImageTitle(preg_replace('/\s+/', '_', $data->description));
            $config->setImageDescription($data->description);
        }
        $config->setImageUrl(json_encode($data->urls));
        $config->setImagePhotographer($data->user->links->html);
        $config->setImageDownloadUrl($data->links->download);
        return $config;

    }

}
