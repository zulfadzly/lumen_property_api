<?php

namespace CustomLibraries\Utilities\DataProcessor\Country;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use CustomLibraries\Utilities\DataProvider\CountryDataJsonFile\CountryDataJsonFile;

class CountryProcessor {

    const CATEGORIES_TABLE = 'categories';
    const COUNTRY_CATEGORY = 'country';
    const STATE_CATEGORY = 'state';
    const DISTRICT_CATEGORY = 'district';
    private mixed $createdAt;
    private mixed $updatedAt;
    private string $jsonFilePath = __DIR__ . '/../../../../database/seeders/country-state-city.json';


    public function process() {

        $this->setCreatedAt(Carbon::now());
        $this->setUpdatedAt(Carbon::now());

        $initCountriesData = [];
        $initStatesData = [];
        $initDistrictData = [];
        $categoryObject = [
            'category' => '',
            'main_category_id' => 0,
            'name' => '',
            "created_at" =>  $this->getCreatedAt(),
            "updated_at" => $this->getUpdatedAt()
        ];

        $countryDataProvider = New CountryDataJsonFile($this->getJsonFilePath());
        $countries = $countryDataProvider->getCountry();

        //Get country list
        foreach ($countries as $key => $value) {

            if(!$this->isCountryExist($value)) {

                $categoryObject['category'] = SELF::COUNTRY_CATEGORY ;
                $categoryObject['name'] = $value;
                $initCountriesData[] = $categoryObject;

            }

        }

        //Store country list
        DB::table('categories')
            ->insert($initCountriesData);

        $countryInDB = DB::table(SELF::CATEGORIES_TABLE)
            ->select('id' ,'name')
            ->where('category', '=', SELF::COUNTRY_CATEGORY)
            ->get();

        foreach ($countryInDB as $country) {

            $countryName = strtolower($country->name);

            $normalStates = $countryDataProvider->getState($countryName);
            $federalStates = $countryDataProvider->getState($countryName,"federal_territories");
            $states = array_merge($normalStates, $federalStates);

            foreach ($states as $key => $value) {

                if(!$this->isStateExist($country->id ,$value)) {

                    $categoryObject['category'] = SELF::STATE_CATEGORY;
                    $categoryObject['main_category_id'] = $country->id;
                    $categoryObject['name'] = $value;
                    $initStatesData[] = $categoryObject;

                }

            }

        }

        //Store state list
        DB::table('categories')
            ->insert($initStatesData);

        $stateInDB = DB::table(SELF::CATEGORIES_TABLE)
            ->select('id' ,'main_category_id' ,'name')
            ->where('category', '=', SELF::STATE_CATEGORY)
            ->get();

        foreach ($countryInDB as $country) {

            $countryName = strtolower($country->name);

            foreach ($stateInDB as $state) {

                $stateId = $state->id;
                $stateName = strtolower($state->name);
                $normalDistricts = $countryDataProvider->getDistrict($countryName ,$stateName ,$stateId);
                $federalDistricts = $countryDataProvider->getDistrict($countryName ,$stateName ,$stateId,"federal_territories");
                $districts = array_merge($normalDistricts, $federalDistricts);

                foreach ($districts as $key => $value) {

                    if(!$this->isDistrictExist($stateId ,$value)) {

                        $categoryObject['category'] = SELF::DISTRICT_CATEGORY;
                        $categoryObject['main_category_id'] = $stateId;
                        $categoryObject['name'] = $value;
                        $initDistrictData[] = $categoryObject;

                    }

                }

            }

        }

        //Store state list
        DB::table('categories')
            ->insert($initDistrictData);

    }

    /**
     * @return string
     */
    private function getJsonFilePath() : string
    {
        return $this->jsonFilePath;
    }

    /**
     * @param string $jsonFilePath
     */
    private function setJsonFilePath(string $jsonFilePath)
    {
        $this->jsonFilePath = $jsonFilePath;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt() : mixed
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(mixed $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt() : mixed
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt(mixed $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param string $country
     * @return bool
     */
    private function isCountryExist(string $country) : bool {

        $country = DB::table(SELF::CATEGORIES_TABLE)
            ->select(DB::raw('COUNT(id) AS cnt'))
            ->where('category', '=', SELF::COUNTRY_CATEGORY)
            ->where('name', '=', $country)
            ->get();
        return $country->first()->cnt > 0;

    }

    /**
     * @param int $countryId
     * @param string $state
     * @return bool
     */
    private function isStateExist(int $countryId, string $state) : bool {

        $state = DB::table(SELF::CATEGORIES_TABLE)
            ->select(DB::raw('COUNT(id) AS cnt'))
            ->where('category', '=', SELF::STATE_CATEGORY)
            ->where('name', '=', $state)
            ->where('main_category_id', '=', $countryId)
            ->get();
        return $state->first()->cnt > 0;

    }

    /**
     * @param int $stateId
     * @param string $district
     * @return bool
     */
    private function isDistrictExist(int $stateId, string $district) : bool {

        $district = DB::table(SELF::CATEGORIES_TABLE)
            ->select(DB::raw('COUNT(id) AS cnt'))
            ->where('category', '=', SELF::DISTRICT_CATEGORY)
            ->where('name', '=', $district)
            ->where('main_category_id', '=', $stateId)
            ->get();
        return $district->first()->cnt > 0;

    }

}
