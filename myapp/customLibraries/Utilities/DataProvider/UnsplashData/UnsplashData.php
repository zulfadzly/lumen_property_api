<?php

namespace CustomLibraries\Utilities\DataProvider\UnsplashData;

use CustomLibraries\Utilities\DataProvider\Interfaces\PropertyImageDataProvider;
use Unsplash\HttpClient;
use Unsplash\Search;
use stdClass;

class UnsplashData implements PropertyImageDataProvider
{
    const DATA_PAGE = 1;
    const DATA_PER_PAGE = 20;
    const DATA_IMAGE_ORIENTATION_LANDSCAPE = 'landscape';
    private int $page = SELF::DATA_PAGE;
    private int $perPage = SELF::DATA_PER_PAGE;
    private string $orientation = SELF::DATA_IMAGE_ORIENTATION_LANDSCAPE;


    public function getImages($imageKeyword) : stdClass
    {

        HttpClient::init([
            'applicationId'	=> env('UNSPLASH_APPLICATION_ID'),
            'secret'	=> env('UNSPLASH_SECRET'),
            'callbackUrl'	=> env('UNSPLASH_CALLBACK_URL'),
            'utmSource' => env('UNSPLASH_UTM_SOURCE')
        ]);

        $search = $imageKeyword;
        $page = $this->getPage();
        $per_page = $this->getPerPage();
        $orientation = $this->getOrientation();
        $data = Search::photos($search, $page, $per_page, $orientation);
        return $this->convertToObject($data->getResults());
        /////////////////////Debug//////////////////////////
        //$data = json_decode(file_get_contents(__DIR__ . '/../../../../database/seeders/unsplash_sample.json') ,true);
        //return $this->convertToObject($data['results']);

    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }

    /**
     * @return string
     */
    public function getOrientation(): string
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     */
    public function setOrientation(string $orientation): void
    {
        $this->orientation = $orientation;
    }


    /**
     * @param array $array
     * @return object
     */
    private function convertToObject(array $array) : object
    {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = $this->convertToObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }
}
