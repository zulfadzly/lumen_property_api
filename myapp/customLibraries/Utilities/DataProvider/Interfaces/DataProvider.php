<?php

namespace CustomLibraries\Utilities\DataProvider\Interfaces;

interface DataProvider
{
    public function getData();
}
