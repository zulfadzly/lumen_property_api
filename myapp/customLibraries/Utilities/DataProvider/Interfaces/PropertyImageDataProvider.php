<?php

namespace CustomLibraries\Utilities\DataProvider\Interfaces;

use stdClass;

interface PropertyImageDataProvider
{

    public function getImages(string $imageKeyword) : stdClass;


}
