<?php

namespace CustomLibraries\Utilities\DataProvider\CountryDataJsonFile;

use CustomLibraries\Utilities\DataProvider\Interfaces\DataProvider;
use Exception;

class CountryDataJsonFile implements DataProvider
{

    private string $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function getData() : array
    {

        $data =  json_decode(file_get_contents($this->path), true);
        return $data;

    }

    /**
     * @return array
     */
    public function getCountry() : array {

        $data = $this->getData()['data'];
        $countries = [];

        foreach ($data as $dataKey => $dataValue) {

            try {
                $countries[] = $dataValue['country'];

            }
            catch (Exception $exception) {

                echo $exception->getMessage();

            }

        }

        return $countries;
    }

    /**
     * @param string $country
     * @param string $stateKeyword
     * @return array
     */
    public function getState(string $country, string $stateKeyword = 'state') : array {

        $data = $this->getData()['data'];
        $states = [];

        foreach ($data as $dataKey => $dataValue) {

            try {

                if(strtolower($dataValue['country']) == $country) {

                    if(isset($dataValue[$stateKeyword])) {

                        $stateData = $dataValue[$stateKeyword];

                        foreach ($stateData as $stateDatum) {

                            $states[] = $stateDatum['name'];

                        }

                    }

                }

            }
            catch (Exception $e) {

            }

        }

        return $states;
    }

    /**
     * @param string $country
     * @param string $state
     * @param int $stateId
     * @param string $stateKeyword
     * @param string $districtKeyword
     * @return array
     */
    public function getDistrict(string $country, string $state, int $stateId, string $stateKeyword = 'state',
                                string $districtKeyword = 'district') : array {

        $data = $this->getData()['data'];
        $districts = [];

        foreach ($data as $dataKey => $dataValue) {

            try {

                if(strtolower($dataValue['country']) == $country) {

                    if(isset($dataValue[$stateKeyword])) {

                        $stateData = $dataValue[$stateKeyword];

                        foreach ($stateData as $stateDatum) {

                            if(strtolower($stateDatum['name']) == $state){

                                $districtData = $stateDatum[$districtKeyword];

                                foreach ($districtData as $districtDatum) {

                                    $districts[] = $districtDatum['name'];

                                }

                            }

                        }

                    }

                }

            }
            catch (Exception $e) {

            }

        }

        return $districts;
    }

}
