<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreatePropertyImagesTable extends Migration
{

    private mixed $createdAt;
    private mixed $updatedAt;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->setCreatedAt(Carbon::now());
        $this->setUpdatedAt(Carbon::now());

        Schema::create('property_images', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->uuid('image_id');
            $table->integer('property_type_id');
            $table->longText('title');
            $table->longText('description');
            $table->json('url');
            $table->longText('photographer_attribution_url');
            $table->longText('download_url');
            $table->timestamps();
        });

        $imageReflection = new ReflectionClass("CustomLibraries\Utilities\DataProcessor\PropertyImage\PropertyImageProcessor");
        try {

            $imageReflectionObjects = $imageReflection->newInstanceWithoutConstructor();
            $imageReflectionObjects->process();


        } catch (ReflectionException $exception) {

            echo $exception->getMessage();

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_images');
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): mixed
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(mixed $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt(): mixed
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt(mixed $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

}
