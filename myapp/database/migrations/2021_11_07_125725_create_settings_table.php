<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CreateSettingsTable extends Migration
{

    private mixed $createdAt;
    private mixed $updatedAt;

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        $this->setCreatedAt(Carbon::now());
        $this->setUpdatedAt(Carbon::now());

        Schema::create('settings', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->tinyText('category');
            $table->smallInteger('main_setting_id');
            $table->tinyText('name');
            $table->timestamps();
        });

        $initData = [
            [
                'category' => 'show_status',
                'main_setting_id' => 0,
                'name' => 'show',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'category' => 'show_status',
                'main_setting_id' => 0,
                'name' => 'hide',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'category' => 'show_status',
                'main_setting_id' => 0,
                'name' => 'pending',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'category' => 'show_status',
                'main_setting_id' => 0,
                'name' => 'delete',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'category' => 'property_intent',
                'main_setting_id' => 0,
                'name' => 'sale',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'category' => 'property_intent',
                'main_setting_id' => 0,
                'name' => 'rent',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ]
        ];

        DB::table('settings')
            ->insert($initData);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }

    /**
     * @return mixed
     */
    public function getCreatedAt() : mixed
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(mixed $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt() : mixed
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt(mixed $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

}
