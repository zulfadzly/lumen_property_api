<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateCategoriesTable extends Migration
{

    private mixed $createdAt;
    private mixed $updatedAt;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->setCreatedAt(Carbon::now());
        $this->setUpdatedAt(Carbon::now());

        Schema::create('categories', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->tinyText('category');
            $table->smallInteger('main_category_id');
            $table->tinyText('name');
            $table->timestamps();
        });

        //Create initial data for Country ,State and District
        $countryReflection = new ReflectionClass("CustomLibraries\Utilities\DataProcessor\Country\CountryProcessor");
        try {

            $countryReflectionObjects = $countryReflection->newInstanceWithoutConstructor();
            $countryReflectionObjects->process();

        } catch (ReflectionException $e) {
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }

    /**
     * @return mixed
     */
    public function getCreatedAt() : mixed
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(mixed $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt() : mixed
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt(mixed $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

}
