<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CreatePropertyTypeTable extends Migration
{

    private mixed $createdAt;
    private mixed $updatedAt;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->setCreatedAt(Carbon::now());
        $this->setUpdatedAt(Carbon::now());

        Schema::create('property_type', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->tinyText('name');
            $table->longText('description');
            $table->timestamps();
        });

        $initData = [
            [
                'name' => 'bungalow',
                'description' => 'are similar to mansions, but slightly smaller in terms of square foot and land.
                The house is surrounded by gardens.',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'name' => 'semi-detached',
                'description' => 'A semi-detached and a detached house are two property types which commonly get used
                interchangeably',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'name' => 'terrace',
                'description' => 'Terrace homes are the smallest among them all, typically 22 ft x 75 ft',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'name' => 'townhouse',
                'description' => 'A townhouse is generally two homes in one, whereby two units will be stacked on top
                of each other or attached side by side. But overall, it just looks like a single home from the
                outside.',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'name' => 'apartment',
                'description' => 'apartment is gated and guarded and is over five storeys tall. It comes with lifts
                and are attached to an outdoor parking space. They have basic facilities such as playgrounds and
                landscaping.',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'name' => 'condominiums',
                'description' => 'Condominiums offers a full range of facilities, high security and indoor or basement
                parking bays. Among the criteria to be fulfilled before a development can be considered as a condominium
                is that it has to be developed in a land area of 4,000 square metres and above',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ],
            [
                'name' => 'penthouse',
                'description' => 'A Penthouse refers to the topmost unit of a residential high rise, and typically
                takes up an entire floor - or at least half the floor.Although in previous developments penthouses were
                single-storey, the trends have changed and now most Penthouses are double-storey homes.There is usually
                only a single Penthouse within a single block of high-rise, making it the most expensive unit.',
                "created_at" =>  $this->getCreatedAt(),
                "updated_at" => $this->getUpdatedAt()
            ]
        ];

        DB::table('property_type')
            ->insert($initData);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_type');
    }

    /**
     * @return mixed
     */
    public function getCreatedAt() : mixed
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(mixed $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt() : mixed
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt(mixed $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

}
