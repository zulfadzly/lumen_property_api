<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->smallInteger('property_type_id');
            $table->smallInteger('country_id');
            $table->smallInteger('state_id');
            $table->smallInteger('district_id');
            $table->tinyText('headline');
            $table->longText('description');
            $table->tinyText('address');
            $table->mediumInteger('image_id');
            $table->decimal('latitude' ,10,8);
            $table->decimal('longitude' ,11,8);
            $table->smallInteger('number_of_bedroom');
            $table->smallInteger('number_of_bathroom');
            $table->decimal('price' ,6,4);
            $table->smallInteger('intent_id');
            $table->smallInteger('show_status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
