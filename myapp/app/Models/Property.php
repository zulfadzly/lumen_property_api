<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_type_id',
        'country_id',
        'state_id',
        'district_id',
        'headline',
        'description',
        'address',
        'image_id',
        'latitude',
        'longitude',
        'number_of_bedroom',
        'number_of_bathroom',
        'price',
        'show_status_id',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
