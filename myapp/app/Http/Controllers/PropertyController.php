<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

/**
 *
 */
class PropertyController extends Controller
{

    //TODO:
    // response type hinting
    // update PHPDOC
    // Validation
    // Unit test
    // Move response message to translation

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getAllProperties()
    {

        $properties = Property::get()
            ->toJson(JSON_PRETTY_PRINT);
        return response($properties, 200);

    }

    //TODO:
    // response type hinting
    // Move response message to translation
    // updatePHPDOC
    // Unit test
    /**
     * @param int $id
     * @return JsonResponse|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getProperties(int $id)
    {

        if (Property::where('id', $id)->exists()) {

            $property = Property::where('id', $id)
                ->get()
                ->toJson(JSON_PRETTY_PRINT);
            return response($property, 200);

        } else {

            return response()->json([
                "message" => "Property not found"
            ], 404);

        }

    }

    //TODO:
    // response type hinting
    // Move response message to translation
    // update PHPDOC
    // Unit test
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {

        Property::create($request->all());
        return response()->json([
            "message" => "Property record created"
        ], 201);

    }

    //TODO:
    // response type hinting
    // Move response message to translation
    // update PHPDOC
    // Unit test
    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(int $id, Request $request)
    {

        $author = Property::findOrFail($id);
        $author->update($request->all());
        return response()->json([
            "message" => "Property record updated"
        ], 200);

    }

    //TODO:
    // response type hinting
    // Move response message to translation
    // update PHPDOC
    // Unit test
    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id)
    {
        Property::findOrFail($id)->delete();
        return response()->json([
            "message" => "Property deleted successfully"
        ], 200);
    }
}
